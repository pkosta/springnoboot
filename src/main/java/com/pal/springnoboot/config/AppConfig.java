package com.pal.springnoboot.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import com.pal.springnoboot.service.BeanConfigWithJavaConfigService;
import com.pal.springnoboot.service.HelloSpringNoBootService;

@Configuration
@PropertySource("classpath:application.properties")
public class AppConfig {

	@Value("${app.devName}")
	private String devName;
	
	@Bean
	public BeanConfigWithJavaConfigService provideJavaConfigService() {
		return new BeanConfigWithJavaConfigService(devName);
	}
	
	@Bean
	public HelloSpringNoBootService provideHelloService() {
		return new HelloSpringNoBootService();
	}
}
