package com.pal.springnoboot.service;

public class BeanConfigWithJavaConfigService {

	public BeanConfigWithJavaConfigService(String name) {
		System.out.println("BeanConfigWithJavaConfigService"+ name);
	}
	
	public void provideMessage() {
		System.out.println("Hello dev! this is spring project but without boot starter");
	}
	
}
