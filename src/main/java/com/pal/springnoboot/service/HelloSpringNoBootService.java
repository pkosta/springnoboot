package com.pal.springnoboot.service;

import org.springframework.beans.factory.annotation.Autowired;

public class HelloSpringNoBootService {
	
	@Autowired
	private BeanConfigWithJavaConfigService javaConfigService;

	public HelloSpringNoBootService() {
		System.out.println("HelloSpringNoBootService");
	}
	
	public void provideMessage() {
		System.out.println("Hello dev! this is spring project but without boot starter");
	}
	
	public void triggerFlow() {
		javaConfigService.provideMessage();
	}
	
}
