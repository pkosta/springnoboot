package com.pal.springnoboot;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.pal.springnoboot.config.AppConfig;
import com.pal.springnoboot.service.HelloSpringNoBootService;

public class SpringnobootApplication {

	public static void main(String[] args) {
		System.out.println("Spring application without boot started");
		
		ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
		HelloSpringNoBootService service = context.getBean(HelloSpringNoBootService.class);
		
		service.provideMessage();
		
		service.triggerFlow();
		
	}

}
